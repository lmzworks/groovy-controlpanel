package works.lmz.controlpanel.panels

import groovy.transform.CompileStatic
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.controlpanel.DefaultControlPanelAssets
import works.lmz.controlpanel.core.ControlPanel
import works.lmz.controlpanel.core.ControlPanelAssets
import works.lmz.controlpanel.core.ControlPanelMetadata
import works.lmz.stencil.LinkBuilder
import works.lmz.controlpanel.bus.GroovyShellExecEventHandler

import javax.inject.Inject


@SingletonBean
@CompileStatic
class GroovyShellContolpanel implements ControlPanel {

	@Inject LinkBuilder linkBuilder
	@Inject DefaultControlPanelAssets defaultControlPanelAssets

	/**
	 * @return the control panel meta data
	 */
	@Override
	ControlPanelMetadata getMetadata() {

		return new ControlPanelMetadata(
				title: "Groovy Admin Console",
				description: "Admin console for running groovy script",
				uri: "admin-console",
				assets: new ControlPanelAssets(
						javascripts: ['/groovyshell/js/groovyshell.js'],
						stylesheets: ['/groovyshell/css/groovyshell.css'],
						embeds: [defaultControlPanelAssets as ControlPanelAssets])
		);
	}

	/**
	 * @return the template to render
	 */
	@Override
	String getTemplate() {
		return "/groovyshell/jsp/groovyshell.jsp";
	}

	/**
	 * @return current user information and table of available fake users
	 */
	@Override
	Map<String, Object> getViewModel() {
		String hinttext = """
// Groovy Code here

// Implicit variables include:
//    ctx: spring context
"""
		GroovyShellExecEventHandler.defaultBeans.each { String beanName, List<String> data ->
			hinttext += "\n//     ${beanName}: ${data[1]}"
		}

		hinttext += "\n\n// Hint:\n// use listBeans() to see all available beans\n"
		hinttext += "// use listBeanMethods(bean) to see method names\n"

		return [hinttext:hinttext as Object]
	}
}
